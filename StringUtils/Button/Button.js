function testForDependencies()
{
	var failure = false;
	if (typeof console == "undefined")
	{
		throw "No console found."; 
		failure = true;
	}
	else
	{
		if (typeof clickAbstraction == "undefined")
		{
			console.error("No click abstraction function found.");
			failure = true;
		}
		if (typeof addClass == "undefined")
		{
			console.error("No class handling functions found.");
			failure = true;
		}
		if (typeof merge == "undefined")
		{
			console.error("No object merging function found.");
			failure = true;
		}
		if (typeof getId == "undefined")
		{
			console.error("No id generation function found.");
			failure = true;
		}
		if (typeof bind == "undefined")
		{
			console.error("No bind function found.");
			failure = true;
		}
		if (typeof toArray == "undefined")
		{
			console.error("No toArray function found.");
			failure = true;
		}
		if (typeof document.createStyleSheet == "undefined")
		{
			console.error("No dynamic stylesheet creation found.");
			failure = true;
		}
	}
	if (failure)
		return false;
	return true;
}

function findTextNode(obj)
{
	if (typeof obj == 'string')
		obj = document.getElementById(obj);
	var children = obj.childNodes;
	if (typeof children == 'undefined' || children.length == 0)
		return null;
	for (var i = 0;i<children.length;i++)
	{
		var child = children[i];
		console.warn("Child" + child.id);
		if (child.nodeType == 3)
		{
			return child.textContent;
		}
		else
		{
			var deeperFind = findTextNode(child);
			if (deeperFind != null)
				return deeperFind;
		}
	}
	return null;
}

function findIcon(obj)
{
	if (typeof obj == 'string')
		obj = document.getElementById(obj);
	var children = obj.childNodes;
	if (obj.nodeName.toLowerCase() == 'img')
	{
		return obj;
	}
	if (typeof children == 'undefined' || children.length == 0)
		return null;
	for (var i = 0;i<children.length;i++)
	{
		var child = children[i];
		console.warn("Child" + child.id);
		if (child.nodeName.toLowerCase() == 'img')
		{
			return child;
		}
		else
		{
			var deeperFind = findTextNode(child);
			if (deeperFind != null)
				return deeperFind;
		}
	}
	return null;
}

function findJQueryClickFunction(obj)
{
	if (typeof obj == 'string')
	{
		console.debug('passed in id: ' + obj);
		obj = jQuery('#' + obj);
	}
	if (obj.click ) //&& obj.data("events")
	{
		//FIXME: This is returning true even when there is no registered click handler.
		var hasJqueryData = jQuery.hasData(obj);
		if (hasJqueryData)
		{
			return function(){obj.click.call(obj);};
		}
		
	}
	if (typeof children == 'undefined' || children.length == 0)
		return null;
	for (var i = 0;i<children.length;i++)
	{
		var child = children[i];
		console.warn("Child" + child.id);
		if (child.click)
		{
			console.debug('found onclick on child 1.');
			return function(){child.click.call(child);};
		}
		else
		{
			var deeperFind = findJQueryClickFunction(child);
			if (deeperFind != null)
			{
				console.debug('found onclick on child 2.');
				return deeperFind;
			}
		}
	}
	return null;
}

function findClickFunction(obj)
{
	var hasJqueryEvent = false;
	if (jQuery)
		hasJqueryEvent = findJQueryClickFunction(obj);
	if (hasJqueryEvent == null)
	{
		if (typeof obj == 'string')
		{
			console.debug('passed in id: ' + obj);
			obj = document.getElementById(obj);
		}
		var children = obj.childNodes;
		if (typeof obj.onclick != 'undefined')
		{
			console.debug('found onclick returning...');
			return obj.onclick;
		}
		if (typeof children == 'undefined' || children.length == 0)
			return null;
		for (var i = 0;i<children.length;i++)
		{
			var child = children[i];
			console.warn("Child" + child.id);
			if (typeof child.onclick != 'undefined')
			{
				console.debug('found onclick on child 1.');
				return child.onclick;
			}
			else
			{
				var deeperFind = findClickFunction(child);
				if (deeperFind != null)
				{
					console.debug('found onclick on child 2.');
					return deeperFind;
				}
			}
		}
	}
	return null;
}
function findLabel(obj)
{
	console.warn('Calling find Label for id: ' + obj);
	if (typeof obj == 'string')
		obj = document.getElementById(obj);
	var children = obj.childNodes;
	if (obj.nodeName.toLowerCase() == 'input' && obj.type.toLowerCase() == 'button')
	{
		return obj.value;
	}
	else if (typeof children == 'undefined' || children.length == 0)
		return null;

	
	for (var i = 0;i<children.length;i++)
	{
		var child = children[i];
		console.warn("Child Id: " + child.id);
		console.warn("	NodeType: " + child.nodeType);
		console.warn("		NodeName: " + child.nodeName); 
		if (child.nodeType == 3)
		{
			return child.textContent;
		}
		else if (child.nodeName.toLowerCase() == 'input' && child.type.toLowerCase() == 'button')
		{
			return child.value;
		}
		else
		{
			var deeperFind = findLabel(child);
			if (deeperFind != null)
				return deeperFind;
		}
	}
	return null;
}


var Buttons = function()
{
	var obj =
	{
		toButton : function(id)
		{
			console.warn('handling object with id: ' + id);
			var onDom = document.getElementById(id);
			//TODO: Check if object exists.
			//TODO: handle a
			//TODO: handle div button
			//TODO: handle button button
			//TODO: handle input
			//TODO: handle finding existing icon
			var img = findIcon(id);
			var imgSrc = null;
			if (img != null)
				imgSrc = img.src;
			var label = findLabel(id);
			if (label == null)
				label = Buttons.label;
			var click = findClickFunction(id);
			if (click == null)
			{
				console.debug("No click found using default");
				click = Buttons.onclick;
			}
			else
			{
				console.debug(click);
			}
			
			console.warn('initializing button with id: ' + id + " and label: " + label + " and click event: " + click);
			if (onDom)
			{
				onDom.id = onDom.id + "_old";
			}
			var button = new Button(
					{
						id : id,
						label : label,
						onclick : click,
						icon : imgSrc
					});
			if (onDom)
			{
				onDom.parentNode.insertBefore(button.dom, onDom);
				onDom.style.display = 'none';
			}
			return button;
		},
		globalTimeTracker : true, //Toggle to see execution time of widget consoled as debug statement.
		buttonStyle : '',
		label: 'Submit',
		id : function(){return "button_" + getId();},
		onclick : function()
		{
			console.debug('Default onclick event. for id: ' + this.defaultConfig.id);
		},
		wrappedOnclick : function()
		{
			console.warn("Wrapped click.");
			this.onclick();
		},
		onclickScopeIsObject : true, // this changes the default "this" for
									// onclick functions. true (default) =
									// the Button object is returned and
									// false the dom object is returned.
		blockLevel : false
		
	};
	return obj;
}();


/**
 * A Button widget. Calling new returns the Button object with a reference to
 * the dom via the .dom property and takes a config object as detailed in the
 * table below. The Button is a block level element that will behave exactly
 * like a standard div if you apply the float property to it. <br/><b>Config
 * Properties:</b> <style>table td, table th{border:1px solid black;}table
 * th{background-color:#eeeeee}</style> <table style="border:1px solid
 * black;border-collapse:collapse">
 * <tr>
 * <th>Property</th>
 * <th>Description</th>
 * <th>Default</th>
 * </tr>
 * <tr>
 * <td>label</td>
 * <td>The text of the button</td>
 * <td>Submit</td>
 * </tr>
 * <tr>
 * <td>icon</td>
 * <td>The path for the button icon</td>
 * <td>./16-em-plus.png</td>
 * </tr>
 * <tr>
 * <td>buttonStyle</td>
 * <td>Built-in css styles for the button - values: positive, negative,
 * neutral, or standard</td>
 * <td>standard</td>
 * </tr>
 * <tr>
 * <td>onclick</td>
 * <td>The function to perform when clicked</td>
 * <td>A function that writes to the console (if it exists).</td>
 * </tr>
 * <tr>
 * <td>onclickScopeIsObject</td>
 * <td>this changes the default "this" for onclick functions. true == the
 * Button object, false == the dom object</td>
 * <td>true(this == Object)</td>
 * </tr>
 * </table>
 * 
 * @return the Button object with a reference to the dom via the .dom property
 */
function Button(uConfig)
{
	var execStart, execEnd;
	if (Buttons.globalTimeTracker)
		execStart = new Date();
	if (testForDependencies() == false)
	{
		console.error("Button instantiation failed due to missing dependencies. See above console statements that list what is missing.");
		return {};
	}
	var addButtonStyles = function()
	{
		if (!window.hasButtonStyleSheet)
			document.createStyleSheet('button.css');
		window.hasButtonStyleSheet = true;
	};
	addButtonStyles();
	
	var thisRef = this;
	this.props =
	{
		id : null,
		label : null,
		iconPath : null,
		buttonStyle : null,
		onclick : null,
		onclickScopeIsObject : null,
		jqueryUI : null,
		blockLevel: null
	};

	/**
	 * This is a reference to the initial configs passed in by the user upon
	 * instantiation. The props property has the <b>current</b> state and
	 * behavior.
	 */
	this.userConfig = uConfig;
	this.defaultConfig =
	{
		"id" : Buttons.id(),
		"onclick" : Buttons.onclick,
		"onclickScopeIsObject" : Buttons.onclickScopeIsObject,
		"buttonStyle" : Buttons.buttonStyle,
		"icon" : "",
		"label" : "Submit",
		"jqueryUI" : false,
		"blockLevel" : Buttons.blockLevel
	};
	
	// QUESTION: Do we have our setters return the button object for chaining
	// purposes?
	this.label = function(label)
	{
		this.setLabel(label);
	};
	this.setLabel = function(label)
	{
		this.props.label = label;
		this.domParts.span.innerHTML = label;
	};
	this.onclick = function(onclick)
	{
		this.setOnclick(onclick);
	};
	this.setOnclick = function(onclick)
	{
		this.props.onclick = onclick;
		if (this.props.onclickScopeIsObject)
		{
			clickAbstraction(this.domParts.div, bind(this, onclick));
			//this.domParts.div.onclick = bind(this, onclick);
		}
		else
		{
			clickAbstraction(this.domParts.div, onclick);
		}
	};
	this.buttonStyle = function(style)
	{
		this.setButtonStyle(style);
	};
	this.setButtonStyle = function(style)
	{
		if (style == 'positive' || style == 'negative' || style == 'neutral' || style == "" || style == 'standard')
		{
			this.props.buttonStyle = style;
			if (style == 'positive')
			{
				replaceClasses(this.domParts.a, [ 'negative', 'neutral' ], [ 'positive' ]);
			}
			else if (style == 'negative')
			{
				replaceClasses(this.domParts.a, [ 'positive', 'neutral' ], [ 'negative' ]);
			}
			else if (style == 'neutral')
			{
				replaceClasses(this.domParts.a, [ 'positive', 'negative' ], [ 'neutral' ]);
			}
			else
			{
				replaceClasses(this.domParts.a, [ 'positive', 'negative', 'neutral' ], []);
			}
		}
		else
		{
			replaceClasses(this.domParts.a, [ 'positive', 'negative', 'neutral' ], [ style ]);
		}
	};

	/* Private methods */

	var applyConfigToDOM = function(wrapper, div, a, img, span, config)
	{
		if (config.id)
		{
			div.id = config.id + "_actual";
			thisRef.props.id = config.id;
		}
		if (config.blockLevel)
		{
			if (config.blockLevel == true)
				wrapper.style.display = 'block';
			else
				wrapper.style.display = 'inline';
			thisRef.props.blockLevel = config.blockLevel;
		}
		else
		{
			wrapper.style.display = 'inline';
		}
		if (config.onclickScopeIsObject)
			thisRef.props.onclickScopeIsObject = config.onclickScopeIsObject;
		if (config.onclick)
		{
			if (config.onclickScopeIsObject)
			{
				//div.onclick = bind(thisRef, config.onclick);
				clickAbstraction(div, bind(thisRef, config.onclick));
			}
			else
			{
				//div.onclick = config.onclick;
				clickAbstraction(div, config.onclick);
			}
			thisRef.props.onclick = config.onclick;
		}
		if (config.label)
		{
			span.innerHTML = config.label;
			thisRef.props.label = config.label;
		}
		if (config.icon)
		{
			img.src = config.icon;
			thisRef.props.iconPath = config.icon;
		}
		if (config.buttonStyle)
		{
			thisRef.setButtonStyle(config.buttonStyle);
			thisRef.props.buttonStyle = config.buttonStyle;
		}

	};

	var buildDOM = function(config)
	{
		var wrapper = document.createElement('div');
		var div = document.createElement('span');
		var a = document.createElement('a');
		var img = document.createElement('img');
		var span = document.createElement('span');
		var clearDiv = document.createElement('div');
		
		if (!config.jqueryUI)
			addClass(div, 'buttons');
		if (config.jqueryUI)
		{
			div.onmousedown = function(){addClass(a, "ui-state-active");};
			div.onmouseup = function(){removeClass(a, "ui-state-active");};
			a.onmouseover = function(){addClass(this, "ui-state-hover");};
			a.onmouseout = function(){removeClass(this, "ui-state-hover"); removeClass(this, "ui-state-active");};
			addClass(a, 'ui-button ui-widget ui-state-default ui-corner-all');
		}
		
		thisRef.domParts =
		{
			div : div,
			a : a,
			img : img,
			span : span
		};
		
		if (config.id)
			wrapper.id = config.id;
		addClass(wrapper, "buttonsWrapper");
		wrapper.style.verticalAlign = 'bottom';
		
		div.appendChild(a);
		a.appendChild(img);
		a.appendChild(span);
		
		clearDiv.style.clear = 'both';
		clearDiv.style.display = 'inline';
		div.appendChild(clearDiv);
		applyConfigToDOM(wrapper, div, a, img, span, config);
		if (img.src == "")
		{
			img.style.display = 'none';
		}

		wrapper.appendChild(div);
		return wrapper;
	};

	var instanceOf = function()
	{
		for(x in thisRef.userConfig)
		{
			if (typeof thisRef.defaultConfig[x] == 'undefined')
			{
				console.error('ERROR: There is no option on button called: ' + x);
			}
		}
		console.warn(typeof thisRef.defaultConfig[x]);
		var extendedConfig = merge(thisRef.defaultConfig, thisRef.userConfig);
		console.warn(typeof thisRef.defaultConfig[x]);
		return buildDOM(extendedConfig);
	};
	this.dom = instanceOf();
	if (Buttons.globalTimeTracker)
	{
		execEnd = new Date();
		console.debug("Button Widget(id: "+this.props.id+") execution time: " + (execEnd.getTime() - execStart.getTime()) + "ms");
	}
	return this;
};