
function testForDependencies()
{var failure=false;if(typeof console=="undefined")
{throw"No console found.";failure=true;}
else
{if(typeof clickAbstraction=="undefined")
{console.error("No click abstraction function found.");failure=true;}
if(typeof addClass=="undefined")
{console.error("No class handling functions found.");failure=true;}
if(typeof merge=="undefined")
{console.error("No object merging function found.");failure=true;}
if(typeof getId=="undefined")
{console.error("No id generation function found.");failure=true;}
if(typeof bind=="undefined")
{console.error("No bind function found.");failure=true;}
if(typeof toArray=="undefined")
{console.error("No toArray function found.");failure=true;}
if(typeof document.createStyleSheet=="undefined")
{console.error("No dynamic stylesheet creation found.");failure=true;}}
if(failure)
return false;return true;}
var Buttons=function()
{var obj={toButton:function(id)
{console.warn('handling object with id: '+id);return new Button({id:id});},globalTimeTracker:true,buttonStyle:'',id:function(){return"button_"+getId();},onclick:function()
{console.debug('Default onclick event. for id: '+this.defaultConfig.id);},onclickScopeIsObject:true,blockLevel:false};return obj;}();function Button(uConfig)
{var execStart,execEnd;if(Buttons.globalTimeTracker)
execStart=new Date();if(testForDependencies()==false)
{console.error("Button instantiation failed due to missing dependencies. See above console statements that list what is missing.");return{};}
var addButtonStyles=function()
{if(!window.hasButtonStyleSheet)
document.createStyleSheet('button.css');window.hasButtonStyleSheet=true;};addButtonStyles();var thisRef=this;this.props={id:null,label:null,iconPath:null,buttonStyle:null,onclick:null,onclickScopeIsObject:null,blockLevel:null};this.userConfig=uConfig;this.defaultConfig={"id":Buttons.id(),"onclick":Buttons.onclick,"onclickScopeIsObject":Buttons.onclickScopeIsObject,"buttonStyle":Buttons.buttonStyle,"icon":"","label":"Submit","jqueryUI":false,"blockLevel":Buttons.blockLevel};this.testFunction=function()
{console.warn('test');};this.label=function(label)
{this.setLabel(label);};this.setLabel=function(label)
{this.props.label=label;this.domParts.span.innerHTML=label;};this.onclick=function(onclick)
{this.setOnclick(onclick);};this.setOnclick=function(onclick)
{this.props.onclick=onclick;if(this.props.onclickScopeIsObject)
{clickAbstraction(this.domParts.div,bind(this,onclick));}
else
{clickAbstraction(this.domParts.div,onclick);}};this.buttonStyle=function(style)
{this.setButtonStyle(style);};this.setButtonStyle=function(style)
{if(style=='positive'||style=='negative'||style=='neutral'||style==""||style=='standard')
{this.props.buttonStyle=style;if(style=='positive')
{replaceClasses(this.domParts.a,['negative','neutral'],['positive']);}
else if(style=='negative')
{replaceClasses(this.domParts.a,['positive','neutral'],['negative']);}
else if(style=='neutral')
{replaceClasses(this.domParts.a,['positive','negative'],['neutral']);}
else
{replaceClasses(this.domParts.a,['positive','negative','neutral'],[]);}}
else
{replaceClasses(this.domParts.a,['positive','negative','neutral'],[style]);}};var applyConfigToDOM=function(wrapper,div,a,img,span,config)
{if(config.id)
{div.id=config.id+"_actual";thisRef.props.id=config.id;}
if(config.blockLevel)
{if(config.blockLevel==true)
wrapper.style.display='block';else
wrapper.style.display='inline';thisRef.props.blockLevel=config.blockLevel;}
else
{wrapper.style.display='inline';}
if(config.onclickScopeIsObject)
thisRef.props.onclickScopeIsObject=config.onclickScopeIsObject;if(config.onclick)
{if(config.onclickScopeIsObject)
{clickAbstraction(div,bind(thisRef,config.onclick));}
else
{clickAbstraction(div,config.onclick);}
thisRef.props.onclick=config.onclick;}
if(config.label)
{span.innerHTML=config.label;thisRef.props.label=config.label;}
if(config.icon)
{img.src=config.icon;thisRef.props.iconPath=config.icon;}
if(config.buttonStyle)
{thisRef.setButtonStyle(config.buttonStyle);thisRef.props.buttonStyle=config.buttonStyle;}};var buildDOM=function(config)
{var wrapper=document.createElement('div');var div=document.createElement('span');var a=document.createElement('a');var img=document.createElement('img');var span=document.createElement('span');var clearDiv=document.createElement('div');if(!config.jqueryUI)
addClass(div,'buttons');if(config.jqueryUI)
{div.onmousedown=function(){addClass(a,"ui-state-active");};div.onmouseup=function(){removeClass(a,"ui-state-active");};a.onmouseover=function(){addClass(this,"ui-state-hover");};a.onmouseout=function(){removeClass(this,"ui-state-hover");removeClass(this,"ui-state-active");};addClass(a,'ui-button ui-widget ui-state-default ui-corner-all');}
thisRef.domParts={div:div,a:a,img:img,span:span};if(config.id)
wrapper.id=config.id;addClass(wrapper,"buttonsWrapper");wrapper.style.verticalAlign='bottom';div.appendChild(a);a.appendChild(img);a.appendChild(span);clearDiv.style.clear='both';clearDiv.style.display='inline';div.appendChild(clearDiv);applyConfigToDOM(wrapper,div,a,img,span,config);if(img.src=="")
{img.style.display='none';}
wrapper.appendChild(div);return wrapper;};var instanceOf=function()
{var extendedConfig=merge(thisRef.defaultConfig,thisRef.userConfig);return buildDOM(extendedConfig);};this.dom=instanceOf();if(Buttons.globalTimeTracker)
{execEnd=new Date();console.debug("Button Widget(id: "+this.props.id+") execution time: "+(execEnd.getTime()-execStart.getTime())+"ms");}
return this;};