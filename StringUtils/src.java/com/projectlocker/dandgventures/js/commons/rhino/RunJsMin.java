package com.projectlocker.dandgventures.js.commons.rhino;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import com.projectlocker.dandgventures.js.commons.rhino.JSMin.UnterminatedCommentException;
import com.projectlocker.dandgventures.js.commons.rhino.JSMin.UnterminatedRegExpLiteralException;
import com.projectlocker.dandgventures.js.commons.rhino.JSMin.UnterminatedStringLiteralException;

public class RunJsMin
{

	public static void main(String[] args) throws IOException, UnterminatedRegExpLiteralException, UnterminatedCommentException, UnterminatedStringLiteralException
	{
		String dir ="src/Button";
		
		
		File buttonjs = new File(dir + "/Button.js");
		File buttoncss = new File(dir + "/Button.css");
		File[] files = new File[]{};
		
		for (File string : files)
		{
			System.out.println(buttonjs.getAbsolutePath());
			String outputName = buttonjs.getName().replace(".js", "_min.js");
			String outputFilePath = dir + "/" + outputName;
			File buttonminjs = new File(outputFilePath);
			System.out.println(buttonminjs.getAbsolutePath());
			FileOutputStream fos = new FileOutputStream(buttonminjs);
			FileInputStream fis = new FileInputStream(buttonjs);
			new JSMin(fis, fos).jsmin();
			System.out.println(buttonjs.length());
			System.out.println(buttonminjs.length());
		}
		
	}
	
}
