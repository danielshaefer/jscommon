package com.projectlocker.dandgventures.js.commons.rhino;

public class TestSuiteResult 
{
	public TestSuiteResult(String testSuiteName)
	{
		this.testSuiteName = testSuiteName;
	}
	
	private String testSuiteName;
	private int totalTests;
	private int successCount;
	private int failureCount;
	
	public void addTestResult(boolean result)
	{
		totalTests++;
		if (result)
			successCount++;
		else
			failureCount++;
	}
	
	public String getTestSuiteName() {
		return testSuiteName;
	}
	public int getTotalTests() {
		return totalTests;
	}
	public int getSuccessCount() {
		return successCount;
	}
	public int getFailureCount() {
		return failureCount;
	}
	
	
	
}
