package com.projectlocker.dandgventures.js.commons.rhino;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

public class RhinoTestRunner {

	public static void main(String[] args) throws IOException
	   {
		   Context rhinoContext = Context.enter();
		   Scriptable scope = rhinoContext.initStandardObjects();
		   setupContextAndEvaluteStringUtilsClass(rhinoContext, scope);
		  
		   List<TestSuiteResult> testSuiteResults = new ArrayList<TestSuiteResult>();
		   
		   testSuiteResults.add(testLeftPad(rhinoContext, scope));
		   testSuiteResults.add(testRightPad(rhinoContext, scope));
		   testSuiteResults.add(testReverse(rhinoContext, scope));
		   
		   testResultDisplay(testSuiteResults);
	   }

		private static TestSuiteResult testReverse(Context rhinoContext, Scriptable scope) 
		{
			TestSuiteResult tsr = new TestSuiteResult("StringUtils.reverse");
			tsr.addTestResult(testReverseNormal(rhinoContext, scope));
			tsr.addTestResult(testReverseNull(rhinoContext, scope));
			tsr.addTestResult(testReverseSingleCharacter(rhinoContext, scope));
			return tsr;
		}

		private static TestSuiteResult testLeftPad(Context rhinoContext, Scriptable scope) 
		{
			TestSuiteResult tsr = new TestSuiteResult("StringUtils.leftPad");
			tsr.addTestResult(testLeftPadWithCharacter(rhinoContext, scope));
			tsr.addTestResult(testLeftPadWithoutCharacter(rhinoContext, scope));
			tsr.addTestResult(testLeftPadWithStringLongerThanPadding(rhinoContext, scope));
			tsr.addTestResult(testLeftPadWithMultiCharacterPadding(rhinoContext, scope));
			tsr.addTestResult(testLeftPadOnNullValue(rhinoContext, scope));
			return tsr;
		}

		private static TestSuiteResult testRightPad(Context rhinoContext, Scriptable scope) 
		{
			TestSuiteResult tsr = new TestSuiteResult("StringUtils.rightPad");
			tsr.addTestResult(testRightPadWithCharacter(rhinoContext, scope));
			tsr.addTestResult(testRightPadWithoutCharacter(rhinoContext, scope));
			tsr.addTestResult(testRightPadWithStringLongerThanPadding(rhinoContext, scope));
			tsr.addTestResult(testRightPadWithMultiCharacterPadding(rhinoContext, scope));
			tsr.addTestResult(testRightPadOnNullValue(rhinoContext, scope));
			return tsr;
		}
		
		private static boolean testLeftPadWithCharacter(Context rhinoContext, Scriptable scope) 
		{
			String str = "test";
			int padSize = 10;
			String padChar = "z";
			String leftPadResultJS = (String)rhinoContext.evaluateString(scope, "StringUtils.leftPad('"+str+"', "+padSize+", '"+padChar+"')", "<cmd>", 1, null);
			String leftPadResultJava = StringUtils.leftPad(str, padSize, padChar);
			return StringUtils.equals(leftPadResultJava, leftPadResultJS);
		}
		
		private static boolean testLeftPadWithoutCharacter(Context rhinoContext, Scriptable scope) 
		{
			String str = "test";
			int padSize = 10;
			String leftPadResultJS = (String)rhinoContext.evaluateString(scope, "StringUtils.leftPad('"+str+"', "+padSize+")", "<cmd>", 1, null);
			String leftPadResultJava = StringUtils.leftPad(str, padSize);
			return StringUtils.equals(leftPadResultJava, leftPadResultJS);
		}
		
		private static boolean testLeftPadWithStringLongerThanPadding(Context rhinoContext, Scriptable scope) 
		{
			String str = "test";
			int padSize = 3;
			String leftPadResultJS = (String)rhinoContext.evaluateString(scope, "StringUtils.leftPad('"+str+"', "+padSize+")", "<cmd>", 1, null);
			String leftPadResultJava = StringUtils.leftPad(str, padSize);
			return StringUtils.equals(leftPadResultJava, leftPadResultJS);
		}
		private static boolean testLeftPadWithMultiCharacterPadding(Context rhinoContext, Scriptable scope) 
		{
			String str = "test";
			int padSize = 10;
			String padChar = "abc";
			String leftPadResultJS = (String)rhinoContext.evaluateString(scope, "StringUtils.leftPad('"+str+"', "+padSize+", '"+padChar+"')", "<cmd>", 1, null);
			String leftPadResultJava = StringUtils.leftPad(str, padSize, padChar);
			return StringUtils.equals(leftPadResultJava, leftPadResultJS);
		}
		private static boolean testLeftPadOnNullValue(Context rhinoContext, Scriptable scope) 
		{
			String str = null;
			int padSize = 10;
			String padChar = "abc";
			String leftPadResultJS = (String)rhinoContext.evaluateString(scope, "StringUtils.leftPad("+str+", "+padSize+", '"+padChar+"')", "<cmd>", 1, null);
			String leftPadResultJava = StringUtils.leftPad(str, padSize, padChar);
			return StringUtils.equals(leftPadResultJava, leftPadResultJS);
		}
		
		
		private static boolean testRightPadWithCharacter(Context rhinoContext, Scriptable scope) 
		{
			String str = "test";
			int padSize = 10;
			String padChar = "z";
			String RightPadResultJS = (String)rhinoContext.evaluateString(scope, "StringUtils.rightPad('"+str+"', "+padSize+", '"+padChar+"')", "<cmd>", 1, null);
			String RightPadResultJava = StringUtils.rightPad(str, padSize, padChar);
			return RightPadResultJava.equals(RightPadResultJS);
		}
		
		private static boolean testRightPadWithoutCharacter(Context rhinoContext, Scriptable scope) 
		{
			String str = "test";
			int padSize = 10;
			String RightPadResultJS = (String)rhinoContext.evaluateString(scope, "StringUtils.rightPad('"+str+"', "+padSize+")", "<cmd>", 1, null);
			String RightPadResultJava = StringUtils.rightPad(str, padSize);
			return RightPadResultJava.equals(RightPadResultJS);
		}
		
		private static boolean testRightPadWithStringLongerThanPadding(Context rhinoContext, Scriptable scope) 
		{
			String str = "test";
			int padSize = 3;
			String RightPadResultJS = (String)rhinoContext.evaluateString(scope, "StringUtils.rightPad('"+str+"', "+padSize+")", "<cmd>", 1, null);
			String RightPadResultJava = StringUtils.rightPad(str, padSize);
			return RightPadResultJava.equals(RightPadResultJS);
		}
		private static boolean testRightPadWithMultiCharacterPadding(Context rhinoContext, Scriptable scope) 
		{
			String str = "test";
			int padSize = 10;
			String padChar = "abc";
			String RightPadResultJS = (String)rhinoContext.evaluateString(scope, "StringUtils.rightPad('"+str+"', "+padSize+", '"+padChar+"')", "<cmd>", 1, null);
			String RightPadResultJava = StringUtils.rightPad(str, padSize, padChar);
			return RightPadResultJava.equals(RightPadResultJS);
		}
		private static boolean testRightPadOnNullValue(Context rhinoContext, Scriptable scope) 
		{
			String str = null;
			int padSize = 10;
			String padChar = "abc";
			String rightPadResultJS = (String)rhinoContext.evaluateString(scope, "StringUtils.rightPad("+str+", "+padSize+", '"+padChar+"')", "<cmd>", 1, null);
			String rightPadResultJava = StringUtils.rightPad(str, padSize, padChar);
			return StringUtils.equals(rightPadResultJava, rightPadResultJS);
		}
		private static boolean testReverseSingleCharacter(Context rhinoContext, Scriptable scope) 
		{
			String str = "A";
			String reverseResultJS = (String)rhinoContext.evaluateString(scope, "StringUtils.reverse('"+str+"')", "<cmd>", 1, null);
			String reverseResultJava = StringUtils.reverse(str);
			return StringUtils.equals(reverseResultJS, reverseResultJava);
		}

		private static boolean testReverseNull(Context rhinoContext, Scriptable scope) 
		{
			String str = null;
			String reverseResultJS = (String)rhinoContext.evaluateString(scope, "StringUtils.reverse("+str+")", "<cmd>", 1, null);
			String reverseResultJava = StringUtils.reverse(str);
			return StringUtils.equals(reverseResultJS, reverseResultJava);
		}

		private static boolean testReverseNormal(Context rhinoContext, Scriptable scope) 
		{
			String str = "A Test Of Reversal";
			String reverseResultJS = (String)rhinoContext.evaluateString(scope, "StringUtils.reverse('"+str+"')", "<cmd>", 1, null);
			String reverseResultJava = StringUtils.reverse(str);
			return StringUtils.equals(reverseResultJS, reverseResultJava);
		}
		

		private static void setupContextAndEvaluteStringUtilsClass(Context rhinoContext, Scriptable scope) throws IOException 
		{
			File file = new File("src.js/com/jscommons/js/StringUtils.js");
			System.out.println(file.getAbsolutePath());
			String stringUtilsJs = FileUtils.readFileToString(file);
			rhinoContext.evaluateString(scope, stringUtilsJs, "<cmd>", 1, null);
		}
		
		private static void testResultDisplay(List<TestSuiteResult> results)
		{
			int totalFailures = 0;
			for (TestSuiteResult result : results) 
			{
				System.out.println(result.getTestSuiteName() + " results:");
				if (result.getSuccessCount() > 0)
					System.out.println(result.getSuccessCount() + "/" + result.getTotalTests() + " tests Succeeded.");
				if (result.getFailureCount() > 0)
					System.out.println(result.getFailureCount() + "/" + result.getTotalTests() + " tests Failed.");
				totalFailures += result.getFailureCount();
			}
			if (totalFailures == 0)
				System.out.println("\n****ALL TESTS PASSED****");
		}
		
	
}
