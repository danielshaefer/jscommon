function StringUtils()
{
    this.substringBefore = function(str, separator)
    {
         if (!str || !separator) {
            return str;
        }
        if (separator.length == 0) {
            return "";
        }
        var pos = str.indexOf(separator);
        if (pos == -1) {
            return str;
        }
        return str.substring(0, pos);
    };
    this.substringAfter = function(str, separator)
    {
        if (!str || !separator) {
            return "";
        }
        if (separator.length == 0) {
            return "";
        }
        var pos = str.indexOf(separator);
        if (pos == -1) {
            return "";
        }
        return str.substring(pos + separator.length);
    };
    this.substringBetween = function(str, open, close)
    {
        if (str == null || open == null || close == null)
        {
            return null;
        }
        var start = str.indexOf(open);
        if (start != -1)
        {
             var end = str.indexOf(close, start + open.length);
             if (end != -1)
             {
                return str.substring(start + open.length, end);
             }
        }
        return null;
    };
    this.leftPad = function(str, size, padStr)
    {
        if (str == null) {
            return null;
        }
        if (padStr == undefined) {
            padStr = " ";
        }
        var padLen = padStr.length;
        var strLen = str.length;
        var pads = size - strLen;
        if (pads <= 0)
        {
            return str; // returns original String when possible
        }

        if (pads == padLen) {
            return padStr.concat(str);
        } else if (pads < padLen) {
            return padStr.substring(0, pads).concat(str);
        } else {
            var padding = [];
            var padChars = padStr;
            for (var i = 0; i < pads; i++) {
                padding[i] = padChars[i % padLen];
            }
            return padding.concat(str).join('');
        }
    };
    this.rightPad = function(str, size, padStr) {
        if (str == null) {
            return null;
        }
        if (padStr == undefined) {
            padStr = " ";
        }
        var padLen = padStr.length;
        var strLen = str.length;
        var pads = size - strLen;
        if (pads <= 0) {
            return str; // returns original String when possible
        }
        if (pads == padLen) {
            return str.concat(padStr);
        } else if (pads < padLen) {
            return str.concat(padStr.substring(0, pads));
        } else {
            var padding = [];
            var padChars = padStr;
            for (var i = 0; i < pads; i++) {
                padding[i] = padChars[i % padLen];
            }
            return str.concat(padding.join(''));
        }
    };
    this.reverse = function(str)
    {
        if (str == undefined)
            return null;
        return str.split("").reverse().join("");
    };
    this.equalsIgnoreCase = function(str, str2)
    {
        return (str + "").toLowerCase() === (str2 + "").toLowerCase();
    };
};

var StringUtils = new StringUtils();
//console.warn(StringUtils.substringAfter('something:nothing', ':'));
//
//   = complete
//Methods to implement
//
//    IsEmpty/IsBlank - checks if a String contains text
//    Trim/Strip - removes leading and trailing whitespace
//    Equals/EqualsIgnoreCase - compares two strings null-safe
//    startsWith - check if a String starts with a prefix null-safe
//    endsWith - check if a String ends with a suffix null-safe
//    IndexOf/LastIndexOf/Contains - null-safe index-of checks
//    IndexOfAny/LastIndexOfAny/IndexOfAnyBut/LastIndexOfAnyBut - index-of any of a set of Strings
//    ContainsIgnoreCase/ContainsOnly/ContainsNone/ContainsAny - does String contains only/none/any of these characters
//    Substring/Left/Right/Mid - null-safe substring extractions
//    SubstringBefore/SubstringAfter/SubstringBetween - substring extraction relative to other strings
//    Split/Join - splits a String into an array of substrings and vice versa
//    Remove/Delete - removes part of a String
//    Replace/Overlay - Searches a String and replaces one String with another
//    Chomp/Chop - removes the last part of a String
//    LeftPad/RightPad/Center/Repeat - pads a String
//    UpperCase/LowerCase/SwapCase/Capitalize/Uncapitalize - changes the case of a String
//    CountMatches - counts the number of occurrences of one String in another
//    IsAlpha/IsNumeric/IsWhitespace/IsAsciiPrintable - checks the characters in a String
//    DefaultString - protects against a null input String
//    Reverse/ReverseDelimited - reverses a String
//    Abbreviate - abbreviates a string using ellipsis
//    Difference - compares Strings and reports on their differences
//    LevensteinDistance - the number of changes needed to change one String into another
//
