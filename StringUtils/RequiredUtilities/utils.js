if (typeof document.createStyleSheet === 'undefined')
{
	document.createStyleSheet = (function()
	{
		function createStyleSheet(href)
		{
			if (typeof href !== 'undefined')
			{
				var element = document.createElement('link');
				element.type = 'text/css';
				element.rel = 'stylesheet';
				element.href = href;
			}
			else
			{
				var element = document.createElement('style');
				element.type = 'text/css';
			}

			var sheet;
			if (document.head)
			{
				sheet = document.head.insertBefore(element, document.head.firstChild);
			}
			else
			{
				document.getElementsByTagName('head')[0].appendChild(element);
				sheet = document.styleSheets[document.styleSheets.length - 1];
			}
			if (typeof sheet.insertRule === 'undefined')
				sheet.insertRule = addRule;
			else if (typeof sheet.addRule === 'undefined')
				sheet.addRule = addRule;

			if (typeof sheet.removeRule === 'undefined')
				sheet.removeRule = sheet.deleteRule;

			return sheet;
		}

		function addRule(selectorText, cssText, index)
		{
			if (typeof index === 'undefined')
				index = this.cssRules.length;

			this.insertRule(selectorText + ' {' + cssText + '}', index);
		}

		return createStyleSheet;
	})();
}

if (!console)
{
	// build console superlite.
	var div = document.createElement('div');
	div.style.position = 'fixed';
	div.style.bottom = '0px';
	div.style.height = '100px';
	div.style.width = '99%';
	div.style.border = '2px solid red';
	div.style.backgroundColor = '#eeeeee';
	div.style.overflow = 'auto';
	div.style.zIndex = -1; // let other elements sit on top of the window if
							// they have been absolutely positioned to do so.

	var msgFunc = function(type, msg, dom)
	{
		if (!window.consoleLiteAppended)
		{
			document.body.appendChild(div);
			window.consoleLiteAppended = true;
		}
		dom.appendChild(document.createTextNode(type + ": " + msg));
		dom.appendChild(document.createElement('br'));
		dom.scrollTop = dom.scrollHeight;
	};
	var console = {};
	console.warn = function(msg)
	{
		msgFunc("warn", msg, div);
	};
	console.debug = function(msg)
	{
		msgFunc("debug", msg, div);
	};
	console.error = function(msg)
	{
		msgFunc("error", msg, div);
	};
	console.info = function(msg)
	{
		msgFunc("info", msg, div);
	};
}

function toArray(obj)
{
	return Array.prototype.slice.call(obj);
}

// Bind in its simplest form
function bind(scope, fn)
{
	return function()
	{
		return fn.apply(scope, toArray(arguments));
	};
}

// QUESTION: Should each widget have its own id generator?...or is that
// unnecessary bloat?
/**
 * Returns a universally incrementing unique id. Starts with 1.
 */
var getId = (function()
{
	var id = 1;
	return function()
	{
		return id++;
	};
})();

function merge(o1, o2)
{
	// Refer to the link if we want to adapt to frameworks that mess with
	// prototypes.
	// http://stackoverflow.com/questions/171251/how-can-i-merge-properties-of-two-javascript-objects-dynamically
	for (attrname in o2)
	{
		o1[attrname] = o2[attrname];
	}
	return o1;
}
function overwriteMerge(o1, o2)
{
	// Refer to the link if we want to adapt to frameworks that mess with
	// prototypes.
	// http://stackoverflow.com/questions/171251/how-can-i-merge-properties-of-two-javascript-objects-dynamically
	for (attrname in o2)
	{
		o1[attrname] = o2[attrname];
	}
	return o1;
}

function hasClass(ele, cls)
{
	return ele.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
}
function addClass(ele, cls)
{
	if (!this.hasClass(ele, cls))
		ele.className += " " + cls;
}
function removeClass(ele, cls)
{
	if (hasClass(ele, cls))
	{
		var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
		ele.className = ele.className.replace(reg, ' ');
	}
}
function replaceClass(ele, clsToReplace, newCls)
{
	removeClass(ele, clsToReplace);
	addClass(ele, newCls);
}
function replaceClasses(ele, classesToReplace, newClasses)
{
	for ( var i = 0; i < classesToReplace.length; i++)
	{
		removeClass(ele, classesToReplace[i]);
	}
	for ( var i = 0; i < newClasses.length; i++)
	{
		addClass(ele, newClasses[i]);
	}
}

function clickAbstraction(el, fn)
{
	if (typeof jQuery != 'undefined')
		jQuery(el).click(fn);
	else if (typeof Prototype != 'undefined')
		console.warn('detected prototype');
	else
	{
		//base(native) impl
		 el.onclick = fn;
	}
}

