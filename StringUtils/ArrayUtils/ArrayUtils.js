ArrayUtils = {};

ArrayUtils._evalClosure = function(closure, closureArg)
{
	var closureFunction;
	if (typeof(closure) === "string")
	{
		closureFunction = function(it)//'it' becomes an implicit variable in the expression
		{
			return eval(closure);
		};
	}
	else
	{
		closureFunction = closure;
	}
	return closureFunction(closureArg);
};

ArrayUtils.each = function(array, closure)
{
	if (array)
	{
		for (var i = 0; i < array.length; i++)
		{
			ArrayUtils._evalClosure(closure, array[i]);
		}
		return array;
	}
	return null;
};

ArrayUtils.reverseEach = function(array, closure)
{
	if (array)
	{
		for (var i = array.length - 1; i >= 0; i--)
		{
			ArrayUtils._evalClosure(closure, array[i]);
		}
		return array;
	}
	return null;
};

ArrayUtils.collect = function(array, closure)
{
	if (array)
	{
		var toReturn = [];
		for (var i = 0; i < array.length; i++)
		{
			toReturn.push(ArrayUtils._evalClosure(closure, array[i]));
		}
		return toReturn;
	}
	return null;
};

ArrayUtils.find = function(array, closure)
{
	if (array)
	{
		for (var i = 0; i < array.length; i++)
		{
			if (ArrayUtils._evalClosure(closure, array[i]))
				return array[i];
		}
	}
	return null;
};

ArrayUtils.findAll = function(array, closure)
{
	if (array)
	{
		var toReturn = [];
		for (var i = 0; i < array.length; i++)
		{
			if (ArrayUtils._evalClosure(closure, array[i]))
				toReturn.push(array[i]);
		}
		return toReturn;
	}
	return null;
};

ArrayUtils.findIndexOf = function(array, closure)
{
	if (array)
	{
		for (var i = 0; i < array.length; i++)
		{
			if (ArrayUtils._evalClosure(closure, array[i]))
				return i;
		}
		return -1;
	}
	return -1;
};

ArrayUtils.indexOf = function(array, it)
{
	if (array)
	{
		for (var i = 0; i < array.length; i++)
		{
			if (array[i] === it)
				return i;
		}
		return -1;
	}
	return -1;
};

ArrayUtils.contains = function(array, it)
{
	return ArrayUtils.indexOf(array, it) != -1;
};

ArrayUtils.intersect = function(array1, array2)
{
	var toReturn = [];
	if (array1 && array2)
	{
		for (var i = 0; i < array1.length; i++)
		{
			if (!ArrayUtils.contains(toReturn, array1[i]) && ArrayUtils.contains(array2, array1[i]))
				toReturn.push(array1[i]);
		}
	}
	return toReturn;
};

ArrayUtils.runTests = function()
{
	var results = [];
	
	var eachResult = ArrayUtils.testEach();
	results.push(eachResult);
	
	var intersectResult = ArrayUtils.testIntersect();
	results.push(intersectResult);
	

	var failures = 0;
	var summaryOfSuccessVsFailure = "<div>Individual Results:<br/>";
	var comma = "";
	for (var i = 0;i<results.length;i++)
	{
		if (results[i].success == false)
		{
			failures++;
			summaryOfSuccessVsFailure += comma + "<span style='color:red'>" + results[i].name + " failed.</span>";
		}
		else
		{
			summaryOfSuccessVsFailure += comma + "<span style='color:green'>" + results[i].name + " passed.</span>";
		}
		comma = "<br/>";
	}
	summaryOfSuccessVsFailure += "</div>";
	var summary = "<div>";
	if (failures == 0)
	{
		summary += "<span style='color:green'>" + results.length + "/" + results.length + " tests passed.</span>"; 
	}
	else
	{
		summary += "<span style='color:green'>" + (results.length - failures) + "/" + results.length + " tests passed.</span>"; 
		summary += "<span style='color:red;margin-left:10px;'>" + failures + "/" + results.length + " tests failed.</span>"; 
	}
	summary += "</div>";
	return {resultSummary:(summary + summaryOfSuccessVsFailure), results:results};
	
};

ArrayUtils.testEach = function()
{
	var items = [1,3,5];
	var alteredArray = [];
 	var returnedArray = ArrayUtils.each(items, function(it){
 		alteredArray.push( it * 2);
 	});
 	if (alteredArray.join("") == [2, 6, 10].join(""))
 		return {success:true, returnVal:alteredArray, name:"ArrayUtils.each(array, closure)"};
 	else
 		return {success:false, returnVal:alteredArray, name:"ArrayUtils.each(array, closure)", failureReason:"expected [2,6,10] was: " + alteredArray};
};

ArrayUtils.testIntersect = function()
{
	var array1 = [1, 2, 3, 4, 5];
	var array2 = [2, 4, 6, 8, 9];
	var intersection = ArrayUtils.intersect(array1, array2);
	if (intersection.join("") != [2,4].join(""))
		return {success:true, returnVal:intersection, name:"ArrayUtils.intersect(array1, array2)"};
	else
		return {success:false, returnVal:intersection, name:"ArrayUtils.intersect(array1, array2)", failureReason:"expected [2,4] was " + intersection};
};

