## Synopsis and Motivations

JSCommons is small collection of javascript utilities. The first of these is the beginning of an effort to port the Apache StringUtils library over to javascript. This was merely an experiment to see if it was conducive to productivity to have some of the same utilities on both the server and client.
The second is the ArrayUtils class which was a similar effort to port Groovy collection handling syntax into JavaScript. 
The third item, which is by far the most anemic, was an experiement to see just how difficult it would be to build a set of stand-alone(framework free), cross-browser, native JavaScript widgets. The answer: it can be done, but it will be very time consuming...

## Files of Note

The main files to look at are as follows:

1. StringUtils.js (Although the test suite for those in java turned out to be an interesting project in itself...since it was just a utility library that didn't require the DOM, and because we were duplicating functionality from an existing Java library it stood to reason that testing the JavaScript in Java with Rhino would be worthwhile)
2. ArrayUtils.js
3. Button.js/button.html
